### A Pluto.jl notebook ###
# v0.14.1

using Markdown
using InteractiveUtils

# ╔═╡ f98e2e90-dd02-11eb-38d4-c1ed365a039a
using Pkg

# ╔═╡ 49fda275-cd4e-42fc-978b-8c2a43f64f4e
using Symbolics

# ╔═╡ e1b99095-7c4a-45ac-a785-fff3c990017e
using LinearAlgebra

# ╔═╡ b26d2957-7cc8-495c-bf17-ccbd613ce7ae
@variables σ

# ╔═╡ ef83c208-ff96-46f8-a621-e3d30538e621
GameMatrix=[1 2 0 0;
           0 0 2 1]

# ╔═╡ 8e8d123a-8a85-4633-b183-ab8b13d4bd69
#Probability of each outcome
#Player1 Ballet x Player2 Ballet
begin
A=1/3 * 2/3
end

# ╔═╡ ff918d38-41e5-454f-87d3-773e17d6a0c4
B=1/3*1/3

# ╔═╡ 824f142b-0a5c-4ef5-b4d1-5de5efa05c88
C=2/3*2/3

# ╔═╡ 19063261-9de7-44a8-a795-d28d5f052f83
D=2/3*1/3

# ╔═╡ 7dc05ec3-269c-4787-b361-b0a6ec1032ba
#Calculations of each players reward
1*A+0*B+0*C+2*D

# ╔═╡ e6d82f59-1ff6-4876-8a9b-51d14966a8d1
#Calculations of each player2 reward
2*A+0*B+0*C+1*D

# ╔═╡ de9f8ddb-d0ca-434a-a370-03312cdf7256


# ╔═╡ 1b4bc3fe-d0e5-4c0c-8f41-d414f3d91738


# ╔═╡ 05c353a1-a6d0-41f7-b1be-66ff2b3708b0


# ╔═╡ 6cf9e277-c9c3-4ce8-ad92-0fdd2d3ef4b7


# ╔═╡ 17e8ce6b-9b26-4dd4-966d-1446c8fb1551


# ╔═╡ 080cd024-5819-4d7a-b6e6-e4d3fd55e948


# ╔═╡ dcd88a12-c7d5-49f6-a580-e5eb1788fd34


# ╔═╡ f4aeef43-ac01-4f4d-b355-ad9e128d4c92


# ╔═╡ Cell order:
# ╠═f98e2e90-dd02-11eb-38d4-c1ed365a039a
# ╠═49fda275-cd4e-42fc-978b-8c2a43f64f4e
# ╠═e1b99095-7c4a-45ac-a785-fff3c990017e
# ╠═b26d2957-7cc8-495c-bf17-ccbd613ce7ae
# ╠═ef83c208-ff96-46f8-a621-e3d30538e621
# ╠═8e8d123a-8a85-4633-b183-ab8b13d4bd69
# ╠═ff918d38-41e5-454f-87d3-773e17d6a0c4
# ╠═824f142b-0a5c-4ef5-b4d1-5de5efa05c88
# ╠═19063261-9de7-44a8-a795-d28d5f052f83
# ╠═7dc05ec3-269c-4787-b361-b0a6ec1032ba
# ╠═e6d82f59-1ff6-4876-8a9b-51d14966a8d1
# ╠═de9f8ddb-d0ca-434a-a370-03312cdf7256
# ╠═1b4bc3fe-d0e5-4c0c-8f41-d414f3d91738
# ╠═05c353a1-a6d0-41f7-b1be-66ff2b3708b0
# ╠═6cf9e277-c9c3-4ce8-ad92-0fdd2d3ef4b7
# ╠═17e8ce6b-9b26-4dd4-966d-1446c8fb1551
# ╠═080cd024-5819-4d7a-b6e6-e4d3fd55e948
# ╠═dcd88a12-c7d5-49f6-a580-e5eb1788fd34
# ╠═f4aeef43-ac01-4f4d-b355-ad9e128d4c92
